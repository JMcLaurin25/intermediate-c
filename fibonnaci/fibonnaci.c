#include <stdio.h>
#include <stdlib.h>

int fib(int num)
{
	if (num < 2) {
		return num;
	}

	return fib(num - 1) + fib(num - 2);
}

int main (int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <number>\n", argv[0]);
		return 2;
	}

	char *err;
	long value = (int)strtol(argv[1], &err, 10);
	if (*err) {
		fprintf(stderr, "Arg '%s' not a integer\n", argv[1]);
		return 4;
	}

	printf("%d\n", fib(value));
}
