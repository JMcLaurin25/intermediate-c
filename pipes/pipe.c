#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

/* 
 * Set up first element(child) for reading.
 * Set up second element(parent) for writing.
 */

int main(void)
{
	int fd[2]; //File descriptors
	pid_t childpid;

	char *word[] = {"Black", "Yellow"}; // Thing to be read.
	char buf[256]; // Establish the buffer
	int wd_sz =  sizeof(word) / sizeof(*word);
	
	pipe(fd); //Opened pipes - Pipeline established
	/*
	 * Once established, the fd (file descriptor) may be treated
	 * like the descriptors to normal files.
	 */

	if((childpid = fork()) == -1) {
		perror("Fork failed");
		return 1;
	}
	else if (childpid != fork()) {
		//This is the child. It wants to send.
		close(fd[0]);

		int index;

		for(index = 0; index < wd_sz; index++) {
			//Write out to pipe
			write(fd[1], word[index], (strlen(word[index]) + 1));
		}


		close(fd[1]); //Close the other side at the end.
	}
	else {
		//This is the parent. It wants to receive.
		close(fd[1]);

		for(index = 0; index < wd_sz; index++) {
			//Read string from the pipe
			read(fd[0], buf, sizeof(buf));
			printf("Word received: %s\n", buf);
		}



		close(fd[0]); //Close the other side at the end.
	}
	return 0;
}
