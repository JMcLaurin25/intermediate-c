#include <errno.h>
#include <stdio.h>

int main (int argc, char * argv[])
{

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <filename>\n", argv[0]);
		return 2;
	}

	FILE *data;
	data = fopen(argv[1], "r"); //Double quotations designate a pointer
	//valgrind with 'valgrind ./file_io file_io.c'

	if (!data) {
		int ret = errno;

		perror("Could not open file");
		return ret;
	}	

	char buf[256];
	while (fgets(buf, sizeof(buf), data)) {
		printf("%s", buf);
	}

	printf("\n");

	//TODO: Add check for file error
	while (ftell(data) != 1) { //Prints each character in reverse
		fseek(data, -2, SEEK_CUR);
		fgets(buf, 2, data);
		printf("%s", buf);
	}



	fclose(data);
}
