#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main(void)
{
	
	pid_t child = fork();

	if (child < 0) {
		perror("Could not fork");
		return;
	} else if (child) {
		printf("Clean your room, %d!\n", child);
	} else {
		printf("You're not my real dad!\n");
		printf("You're just a big %d!\n", child);
	}

	//printf("Hello peeps!\n");

}
